<p align="center"><a href="https://codecept.io/" target="_blank"><img src="https://codecept.io/logo.svg" width="400" alt="Codeceptjs Logo"></a></p>



# Dekra-AutomationTest



## Getting started

After cloning the project, you can install node modules via:
```
yarn install
```

You can fork or download the project if needed.


To run the test, just write this CLI:
```
npx codeceptjs run
```

Have fun.
