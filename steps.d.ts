/// <reference types='codeceptjs' />
type steps_file = typeof import('./steps_file');
type BuyProductProxy = typeof import('./buyProduct_proxy');

declare namespace CodeceptJS {
  interface SupportObject { 
    I: I, 
    current: any,
    BuyProductProxy: BuyProductProxy,
  }
  interface Methods extends PlaywrightTs {}
  interface I extends ReturnType<steps_file> {}
  namespace Translation {
    interface Actions {}
  }
}
