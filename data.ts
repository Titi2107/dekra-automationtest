export enum SubtitleLabelEnum {
    ACCOUNT_DETAILS = 'ACCOUNT DETAILS',
    PERSONAL_DETAILS = 'PERSONAL DETAILS',
    ADDRESS = 'ADDRESS',
}

export enum DataEnum {
    URL = 'https://www.advantageonlineshopping.com/',
    USERNAME = 'TestUsername',
    EMAIL = 'test@yahoo.fr',
    PASSWORD = 'TestPass21',
    FIRSTNAME = 'Thierry',
    LASTNAME = 'Suy',
    PHONE = '0606060606',
    CITY = 'Paris',
    ADDRESS = '2 Rue Jean Réno',
    NEWADDRESSFIELD = '//input[@name="address"]',
    NEWADDRESS = '10 Rue Jean Réno',
    STATE = 'Aquitaine',
    ZIPCODE = '75002',
    SPEAKERSNAME = 'Bose Soundlink Bluetooth Speaker III',
    SAFEPAYUSERKO = 'AAA',
    SAFEPAYUSEROK = 'AAAAA',
    SAFEPAYPW3NUMBERS = '123',
    SAFEPAYPW3NUMBERS1LETTER = '123a',
    SAFEPAYPW5LETTERS = 'AAAaa',
    SAFEPAYPWLOWER = 'AA123',
    SAFEPAYPWOK = 'Azerty123',
    CHECKOUT = '//*[@id="pay_now_btn_SAFEPAY"]',
}

export enum WelcomeBannerLocatorEnum {
    SEARCH_ICON = '//*[@id="searchSection"]',
    USER_ICON = '//*[@id="menuUserLink"]',
    CART_ICON = '//*[@id="shoppingCartLink"]',
    HELP_ICON = '//*[@id="helpLink"]',
}

export enum LoginModalLocatorEnum {
    ADVANTAGELOGO = '//div[@class="PopUp"]//img[@src="../../css/images/logo.png"]',
    FACEBOOKBTN = '//div[@class="PopUp"]//span[@data-ng-click="singWithFacebook()"]',
    USERNAME = '//div[@class="PopUp"]//input[@name="username"]',
    PASSWORD = '//div[@class="PopUp"]//input[@name="password"]',
    REMEMBER = '//div[@class="PopUp"]//input[@name="remember_me"]',
    SIGNIN = '//div[@class="PopUp"]//button[@id="sign_in_btnundefined"]',
    FORGOTPW = '//div[@class="PopUp"]//a[@data-ng-click="forgotPassword()"]',
    CREATEACC = '//div[@class="PopUp"]//a[@data-ng-click="createNewAccount()"]',
}

export enum RegisterLocatorEnum {
    // Register page
    SUBTITLE = '//div[@id="formCover"]//h3',
    USERNAME = '//div[@id="formCover"]//input[@name="usernameRegisterPage"]',
    EMAIL = '//div[@id="formCover"]//input[@name="emailRegisterPage"]',
    PASSWORD = '//div[@id="formCover"]//input[@name="passwordRegisterPage"]',
    CONFPASSWORD = '//div[@id="formCover"]//input[@name="confirm_passwordRegisterPage"]',
    FIRSTNAME = '//div[@id="formCover"]//input[@name="first_nameRegisterPage"]',
    LASTNAME = '//div[@id="formCover"]//input[@name="last_nameRegisterPage"]',
    PHONE = '//div[@id="formCover"]//input[@name="phone_numberRegisterPage"]',
    COUNTRY = '//div[@id="formCover"]//select[@name="countryListboxRegisterPage"]',
    CITY = '//div[@id="formCover"]//input[@name="cityRegisterPage"]',
    ADDRESS = '//div[@id="formCover"]//input[@name="addressRegisterPage"]',
    STATE = '//div[@id="formCover"]//input[@name="state_/_province_/_regionRegisterPage"]',
    ZIPCODE = '//div[@id="formCover"]//input[@name="postal_codeRegisterPage"]',
    CHECKBOX = '//sec-view[@sec-name="registrationAgreement"]//input',
    REGISTER = '//*[@id="register_btnundefined"]',
}

export enum BuyNowProductLocatorEnum {
    SPEAKERS = '//*[@id="speakersImg"]',
    TABLETS = '//*[@id="tabletsImg"]',
    LAPTOPS = '//*[@id="laptopsImg"]',
    MICE = '//*[@id="miceImg"]',
}

export enum SpeakersPropertiesLocatorEnum {
    BLACK = '//*[@id="productProperties"]//span[@title="BLACK"]',
    GRAY = '//*[@id="productProperties"]//span[@title="GRAY"]',
    PLUS = '//*[@id="productProperties"]//div[@class="plus"]',
    MINUS = '//*[@id="productProperties"]//div[@class="minus"]',
    ADD = '//*[@id="productProperties"]//button[@name="save_to_cart"]',
}

export enum TooltipCartLocatorEnum {
    MODAL = '//*[@id="toolTipCart"]',
    CHECKOUT = '//*[@id="checkOutPopUp"]',
}

export enum OrderPaymentLocatorEnum {
    SHIPPING = '//*[@id="detailslink"]//label[contains(text(), "1. SHIPPING DETAILS ")]',
    PAYMENT = '//*[@id="detailslink"]//label[contains(text(), "2. PAYMENT METHOD")]',
    TOTALTTC = '//*[@id="userCart"]//span[contains(text(), "$589.98")]',
    EDITMODE = '//*[@class="blueLink"]//a[@data-ng-click="setUserDetailsEditMode()"]',
}

export enum PaymentCommonButtonLocatorEnum {
    NEXT = '//*[@id="next_btnundefined"]',
    BACK = '//a[@data-ng-click="backToMainShippingDetails()"]',
}

export enum PaymentMethodLocatorEnum {
    SAFEPAY = '//div[@class="paymentMethods"]//input[@name="safepay" and @checked="checked"]',
    MASTER = '//div[@class="paymentMethods"]//input[@name="masterCredit"]',
    NOTICE = '//div[@ng-show="imgRadioButton == 1"]//h4[@translate="Notice"]',
    USERNAME = '//div[@ng-show="imgRadioButton == 1"]//sec-form[contains(@class, "secForm")]//input[@name="safepay_username"]',
    PASSWORD = '//div[@ng-show="imgRadioButton == 1"]//sec-form[contains(@class, "secForm")]//input[@name="safepay_password"]',
    PAYNOWDISABLED = "//*[@id='pay_now_btn_SAFEPAY' and @disabled]",
}

export enum CommonErrorMessageEnum {
    PREFIX = '//label[contains(text(), ',
    FIVECHAR = 'Use  5 character or longer',
    FOURCHAR = 'Use  4 character or longer',
    UPPERLETTER = 'One upper letter required',
    LOWERLETTER = 'One lower letter required',
    ONENUMBER = 'One number required',
}

export enum SuccessMessageLocatorEnum {
    TRACKINGNUMBER = '//*[@id="trackingNumberLabel"]',
    ORDERNUMBER = '//*[@id="orderNumberLabel"]',
    SUCCESS = '//*[@id="orderPaymentSuccess"]//span[@translate="Thank_you_for_buying_with_Advantage"]',
}