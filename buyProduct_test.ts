import { 
    DataEnum, 
    WelcomeBannerLocatorEnum,
    LoginModalLocatorEnum, 
    RegisterLocatorEnum,
    BuyNowProductLocatorEnum,
    SpeakersPropertiesLocatorEnum,
    TooltipCartLocatorEnum,
    OrderPaymentLocatorEnum,
    PaymentCommonButtonLocatorEnum,
    PaymentMethodLocatorEnum,
    CommonErrorMessageEnum,
    SuccessMessageLocatorEnum,
} from './data';

import BuyProductProxy from './buyProduct_proxy';

Feature('buyProdcut');

Scenario('Dekra test', async ({ I }) => {
    //Go on site and check mandatory elements
    await I.amOnPage(DataEnum.URL);
    Object.values(WelcomeBannerLocatorEnum).map(async icon => {
        await I.seeElement(icon);
    });
    await I.click(WelcomeBannerLocatorEnum.USER_ICON);
    Object.values(LoginModalLocatorEnum).map(async locator => {
        await I.seeElement(locator);
    });
    await I.click(LoginModalLocatorEnum.CREATEACC);
    Object.values(RegisterLocatorEnum).map(async locator => {
        await I.seeElement(locator);
    });
    await BuyProductProxy.CheckSubtitlesValues();
    //Create account - Account Details
    await BuyProductProxy.FillAccountDetails();
    //Personal Details
    await BuyProductProxy.FillPersonalDetails();
    //Address
    await I.click(RegisterLocatorEnum.COUNTRY);
    I.type('fr');
    I.pressKey('Enter');
    await BuyProductProxy.FillAddress();
    await I.click(RegisterLocatorEnum.CHECKBOX);
    await I.click(RegisterLocatorEnum.REGISTER);
    //Connected user - Redirect to main page
    Object.values(BuyNowProductLocatorEnum).map(async product => {
        await I.waitForElement(product, 5);
    });
    //Get speaker
    await I.click(BuyNowProductLocatorEnum.SPEAKERS);
    await I.wait(1);
    await I.click(DataEnum.SPEAKERSNAME);
    await I.wait(1);
    await I.click(SpeakersPropertiesLocatorEnum.GRAY);
    await I.click(SpeakersPropertiesLocatorEnum.PLUS);
    await I.click(SpeakersPropertiesLocatorEnum.ADD);
    await I.waitForElement(TooltipCartLocatorEnum.MODAL, 5);
    await I.click(TooltipCartLocatorEnum.CHECKOUT);
    //Shipping cart
    Object.values(OrderPaymentLocatorEnum).map(async locator => {
        await I.waitForElement(locator, 5);
    });
    I.click(OrderPaymentLocatorEnum.EDITMODE);
    await I.fillField(DataEnum.NEWADDRESSFIELD, DataEnum.NEWADDRESS);
    await I.click(PaymentCommonButtonLocatorEnum.NEXT);
    Object.values(PaymentMethodLocatorEnum).map(async locator => {
        await I.waitForElement(locator, 5);
    });
    //Fill safepay info and regex
    //ID
    await I.wait(2);
    await I.fillField(PaymentMethodLocatorEnum.USERNAME, DataEnum.SAFEPAYUSERKO);
    await I.click(PaymentMethodLocatorEnum.PASSWORD);
    const labelPrefix = CommonErrorMessageEnum.PREFIX;
    I.seeElement(labelPrefix + `'${CommonErrorMessageEnum.FIVECHAR}')]`, 5);
    await I.fillField(PaymentMethodLocatorEnum.USERNAME, DataEnum.SAFEPAYUSEROK);
    //Pwd
    await BuyProductProxy.HandlePasswordRegex(PaymentMethodLocatorEnum.PASSWORD);
    //Finalize
    await I.fillField(PaymentMethodLocatorEnum.PASSWORD, DataEnum.SAFEPAYPWOK);
    await I.dontSeeElement(PaymentMethodLocatorEnum.PAYNOWDISABLED, 5);
    await I.click(DataEnum.CHECKOUT);
    Object.values(SuccessMessageLocatorEnum).map(async successInfo => {
        await I.waitForElement(successInfo, 5);
    });
});