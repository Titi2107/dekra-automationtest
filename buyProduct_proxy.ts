import { 
    CommonErrorMessageEnum, 
    DataEnum, 
    PaymentMethodLocatorEnum, 
    RegisterLocatorEnum, 
    SubtitleLabelEnum
} from "./data";

const { I } = inject();

class BuyProductProxy {

    public async CheckSubtitlesValues(): Promise<void> {
        const prefix = RegisterLocatorEnum.SUBTITLE;
        const account = prefix + `[contains(text(), '${SubtitleLabelEnum.ACCOUNT_DETAILS}')]`;
        const personal = prefix + `[contains(text(), '${SubtitleLabelEnum.PERSONAL_DETAILS}')]`;
        const address = prefix + `[contains(text(), '${SubtitleLabelEnum.ADDRESS}')]`;
        const tab = [account, personal, address];
        tab.map(async value => {
            await I.seeElement(value);
        });
    }

    public async FillAccountDetails(): Promise<void> {
        const date = new Date();
        await I.fillField(RegisterLocatorEnum.USERNAME, DataEnum.USERNAME + date.getUTCMilliseconds());
        await I.fillField(RegisterLocatorEnum.EMAIL, DataEnum.EMAIL);
        await I.fillField(RegisterLocatorEnum.PASSWORD, DataEnum.PASSWORD);
        await I.fillField(RegisterLocatorEnum.CONFPASSWORD, DataEnum.PASSWORD);
    }

    public async FillPersonalDetails(): Promise<void> {
        await I.fillField(RegisterLocatorEnum.FIRSTNAME, DataEnum.FIRSTNAME);
        await I.fillField(RegisterLocatorEnum.LASTNAME, DataEnum.LASTNAME);
        await I.fillField(RegisterLocatorEnum.PHONE, DataEnum.PHONE);
    }

    public async FillAddress(): Promise<void> {
        await I.fillField(RegisterLocatorEnum.CITY, DataEnum.CITY);
        await I.fillField(RegisterLocatorEnum.ADDRESS, DataEnum.ADDRESS);
        await I.fillField(RegisterLocatorEnum.STATE, DataEnum.STATE);
        await I.fillField(RegisterLocatorEnum.ZIPCODE, DataEnum.ZIPCODE);
    }

    public async HandlePasswordRegex(field: string): Promise<void> {
        const password = [
            DataEnum.SAFEPAYPW3NUMBERS,
            DataEnum.SAFEPAYPW3NUMBERS1LETTER,
            DataEnum.SAFEPAYPW5LETTERS,
            DataEnum.SAFEPAYPWLOWER,
        ];
        const expectedError = [
            CommonErrorMessageEnum.PREFIX + `'${CommonErrorMessageEnum.FOURCHAR}')]`,
            CommonErrorMessageEnum.PREFIX + `'${CommonErrorMessageEnum.UPPERLETTER}')]`,
            CommonErrorMessageEnum.PREFIX + `'${CommonErrorMessageEnum.ONENUMBER}')]`,
            CommonErrorMessageEnum.PREFIX + `'${CommonErrorMessageEnum.LOWERLETTER}')]`,
        ];
        for (let i = 0; i < password.length; i++) {
            await I.fillField(field, password[i]);
            await I.click(PaymentMethodLocatorEnum.USERNAME);
            await I.seeElement(expectedError[i], 5);
        }
    }

}

export = new BuyProductProxy();